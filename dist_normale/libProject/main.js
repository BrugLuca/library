(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/_modules/utente.ts":
/*!************************************!*\
  !*** ./src/app/_modules/utente.ts ***!
  \************************************/
/*! exports provided: Utente */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Utente", function() { return Utente; });
var Utente = /** @class */ (function () {
    function Utente() {
    }
    return Utente;
}());



/***/ }),

/***/ "./src/app/_services/autenticazione.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/_services/autenticazione.service.ts ***!
  \*****************************************************/
/*! exports provided: AutenticazioneService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutenticazioneService", function() { return AutenticazioneService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./base.service */ "./src/app/_services/base.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");





var AutenticazioneService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](AutenticazioneService, _super);
    function AutenticazioneService(http) {
        var _this = _super.call(this) || this;
        _this.http = http;
        return _this;
    }
    AutenticazioneService.prototype.login = function (usr, pss) {
        var _this = this;
        var utenteSessione = null;
        var auth = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Observable"](function (observer) {
            var url = _this.costruiscimiUnUrl('utenti?username=' + usr + '&password=' + pss);
            _this.http.get(url).subscribe(function (risposta) {
                if (risposta[0]) {
                    utenteSessione = risposta[0];
                    _this.memorizzaSessioneUsr(utenteSessione);
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
        return auth;
    };
    AutenticazioneService.prototype.memorizzaSessioneUsr = function (usr) {
        localStorage.setItem('currentUser', JSON.stringify(usr));
    };
    AutenticazioneService.prototype.ottieniDaMemoria = function () {
        return JSON.parse(localStorage.getItem('currentUser'));
    };
    AutenticazioneService.prototype.loggato = function () {
        var utente = this.ottieniDaMemoria();
        if (utente && utente.token) {
            return true;
        }
        else {
            return false;
        }
    };
    AutenticazioneService.prototype.logout = function () {
        localStorage.removeItem('currentUser');
    };
    AutenticazioneService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], AutenticazioneService);
    return AutenticazioneService;
}(_base_service__WEBPACK_IMPORTED_MODULE_2__["BaseService"]));



/***/ }),

/***/ "./src/app/_services/base.service.ts":
/*!*******************************************!*\
  !*** ./src/app/_services/base.service.ts ***!
  \*******************************************/
/*! exports provided: BaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseService", function() { return BaseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var BaseService = /** @class */ (function () {
    function BaseService() {
        this.urlServer = 'http://localhost:3000/';
    }
    BaseService.prototype.costruiscimiUnUrl = function (urlInfo) {
        return this.urlServer + urlInfo;
    };
    BaseService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], BaseService);
    return BaseService;
}());



/***/ }),

/***/ "./src/app/_services/guard.service.ts":
/*!********************************************!*\
  !*** ./src/app/_services/guard.service.ts ***!
  \********************************************/
/*! exports provided: GuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuardService", function() { return GuardService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _autenticazione_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./autenticazione.service */ "./src/app/_services/autenticazione.service.ts");




var GuardService = /** @class */ (function () {
    function GuardService(navigatore, autServ) {
        this.navigatore = navigatore;
        this.autServ = autServ;
    }
    GuardService.prototype.canActivate = function (rotta, stato) {
        if (this.autServ.loggato()) {
            var rottaVera = rotta.routeConfig.path;
            if (rottaVera === '/') {
                this.navigatore.navigate(['users']);
            }
            return true;
        }
        alert('errore');
        this.navigatore.navigate(['login']);
        return false;
    };
    GuardService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _autenticazione_service__WEBPACK_IMPORTED_MODULE_3__["AutenticazioneService"]])
    ], GuardService);
    return GuardService;
}());



/***/ }),

/***/ "./src/app/_services/utenti.service.ts":
/*!*********************************************!*\
  !*** ./src/app/_services/utenti.service.ts ***!
  \*********************************************/
/*! exports provided: UtentiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UtentiService", function() { return UtentiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./base.service */ "./src/app/_services/base.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _modules_utente__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_modules/utente */ "./src/app/_modules/utente.ts");





var UtentiService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UtentiService, _super);
    function UtentiService(http) {
        var _this = _super.call(this) || this;
        _this.http = http;
        _this.autenticato = new _modules_utente__WEBPACK_IMPORTED_MODULE_4__["Utente"]();
        _this.token = false;
        return _this;
    }
    UtentiService.prototype.ottieniListUtenti = function (quale) {
        var url = this.costruiscimiUnUrl(quale);
        return this.http.get(url);
    };
    UtentiService.prototype.cercaConId = function (id) {
        return this.ottieniListUtenti('utenti/' + id);
    };
    UtentiService.prototype.logging = function (user, pss) {
        var url = this.costruiscimiUnUrl('utenti?username=' + user + '&password=' + pss);
        return this.http.get(url);
    };
    UtentiService.prototype.editUsr = function (usr) {
        var url = this.costruiscimiUnUrl('utenti/' + usr.id);
        return this.http.put(url, usr);
    };
    UtentiService.prototype.deleteUsr = function (usr) {
        var url = this.costruiscimiUnUrl('utenti/' + usr.id);
        return this.http.delete(url);
    };
    UtentiService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], UtentiService);
    return UtentiService;
}(_base_service__WEBPACK_IMPORTED_MODULE_2__["BaseService"]));



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1                   {text-align: center; background-color: cornflowerblue; color:white}\r\naside                { height:500px;  float:left;box-sizing: border-box;}\r\n.logged              {width: 20%;  border-right:2px solid black; }\r\naside                { width:80%; float: left;}\r\n.notlogged           {width:0px; }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsc0JBQXNCLGtCQUFrQixFQUFFLGdDQUFnQyxFQUFFLFdBQVc7QUFDdkYsdUJBQXVCLFlBQVksR0FBRyxVQUFVLENBQUMsc0JBQXNCLENBQUM7QUFDeEUsc0JBQXNCLFVBQVUsR0FBRyw0QkFBNEIsRUFBRTtBQUNqRSx1QkFBdUIsU0FBUyxFQUFFLFdBQVcsQ0FBQztBQUM5QyxzQkFBc0IsU0FBUyxFQUFFIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoMSAgICAgICAgICAgICAgICAgICB7dGV4dC1hbGlnbjogY2VudGVyOyBiYWNrZ3JvdW5kLWNvbG9yOiBjb3JuZmxvd2VyYmx1ZTsgY29sb3I6d2hpdGV9XHJcbmFzaWRlICAgICAgICAgICAgICAgIHsgaGVpZ2h0OjUwMHB4OyAgZmxvYXQ6bGVmdDtib3gtc2l6aW5nOiBib3JkZXItYm94O31cclxuLmxvZ2dlZCAgICAgICAgICAgICAge3dpZHRoOiAyMCU7ICBib3JkZXItcmlnaHQ6MnB4IHNvbGlkIGJsYWNrOyB9XHJcbmFzaWRlICAgICAgICAgICAgICAgIHsgd2lkdGg6ODAlOyBmbG9hdDogbGVmdDt9XHJcbi5ub3Rsb2dnZWQgICAgICAgICAgIHt3aWR0aDowcHg7IH0iXX0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n    <div  *ngIf=\"autUsr\">\n        <h1> LIBRERIA </h1>\n    <aside [ngClass]=\"{'logged':autUsr, 'notlogged':!autUsr}\"> \n        <app-nav-bar>  </app-nav-bar> \n        <button (click)=\"logout()\"> logOut </button>\n    </aside>\n</div>\n    <aside>\n        <router-outlet  (activate)=\"onActivate()\" ></router-outlet> <!-- $event-->\n    </aside>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_autenticazione_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./_services/autenticazione.service */ "./src/app/_services/autenticazione.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var AppComponent = /** @class */ (function () {
    function AppComponent(autenticazione, rotta) {
        this.autenticazione = autenticazione;
        this.rotta = rotta;
        this.title = 'libProject';
        this.autUsr = false;
        this.autUsr = this.autenticazione.loggato();
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent.prototype.logout = function () {
        this.autenticazione.logout();
        this.rotta.navigate(['login']);
    };
    AppComponent.prototype.onActivate = function () {
        this.autUsr = this.autenticazione.loggato();
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_autenticazione_service__WEBPACK_IMPORTED_MODULE_2__["AutenticazioneService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ "./node_modules/@fortawesome/angular-fontawesome/fesm5/angular-fontawesome.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/fontawesome-svg-core */ "./node_modules/@fortawesome/fontawesome-svg-core/index.es.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _utenti_usr_list_usr_list_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./utenti/usr-list/usr-list.component */ "./src/app/utenti/usr-list/usr-list.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _router_module_router_module_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./router-module/router-module.module */ "./src/app/router-module/router-module.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_utenti_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./_services/utenti.service */ "./src/app/_services/utenti.service.ts");
/* harmony import */ var _utenti_usr_detail_usr_detail_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./utenti/usr-detail/usr-detail.component */ "./src/app/utenti/usr-detail/usr-detail.component.ts");
/* harmony import */ var _services_autenticazione_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./_services/autenticazione.service */ "./src/app/_services/autenticazione.service.ts");
/* harmony import */ var _libri_libri_list_libri_list_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./libri/libri-list/libri-list.component */ "./src/app/libri/libri-list/libri-list.component.ts");
/* harmony import */ var _nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./nav-bar/nav-bar.component */ "./src/app/nav-bar/nav-bar.component.ts");
/* harmony import */ var _prestiti_prestiti_list_prestiti_list_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./prestiti/prestiti-list/prestiti-list.component */ "./src/app/prestiti/prestiti-list/prestiti-list.component.ts");


















var AppModule = /** @class */ (function () {
    function AppModule() {
        _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_6__["library"].add(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["fas"]);
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _utenti_usr_list_usr_list_component__WEBPACK_IMPORTED_MODULE_8__["UsrListComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"],
                _utenti_usr_detail_usr_detail_component__WEBPACK_IMPORTED_MODULE_13__["UsrDetailComponent"],
                _libri_libri_list_libri_list_component__WEBPACK_IMPORTED_MODULE_15__["LibriListComponent"],
                _nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_16__["NavBarComponent"],
                _prestiti_prestiti_list_prestiti_list_component__WEBPACK_IMPORTED_MODULE_17__["PrestitiListComponent"]
            ],
            imports: [
                _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_4__["FontAwesomeModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClientModule"],
                _router_module_router_module_module__WEBPACK_IMPORTED_MODULE_10__["RouterModuleModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"]
            ],
            providers: [_services_utenti_service__WEBPACK_IMPORTED_MODULE_12__["UtentiService"], _services_autenticazione_service__WEBPACK_IMPORTED_MODULE_14__["AutenticazioneService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/libri/libri-list/libri-list.component.css":
/*!***********************************************************!*\
  !*** ./src/app/libri/libri-list/libri-list.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table   {width:100%; margin:0 auto; height:500px; box-sizing: border-box;}\r\ntr,th   {padding:0 5px; box-sizing:border-box; background-color:white; text-align: center; height:10px;}\r\n.odd    {background-color:#FEF}\r\ntr:hover {background: rgba(0,0,0,0.2); box-sizing: border-box; }\r\ntd        {height: 10px;  box-sizing: border-box; }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGlicmkvbGlicmktbGlzdC9saWJyaS1saXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsU0FBUyxVQUFVLEVBQUUsYUFBYSxFQUFFLFlBQVksRUFBRSxzQkFBc0IsQ0FBQztBQUN6RSxTQUFTLGFBQWEsRUFBRSxxQkFBcUIsRUFBRSxzQkFBc0IsRUFBRSxrQkFBa0IsRUFBRSxXQUFXLENBQUM7QUFDdkcsU0FBUyxxQkFBcUI7QUFDOUIsVUFBVSwyQkFBMkIsRUFBRSxzQkFBc0IsRUFBRTtBQUMvRCxXQUFXLFlBQVksR0FBRyxzQkFBc0IsRUFBRSIsImZpbGUiOiJzcmMvYXBwL2xpYnJpL2xpYnJpLWxpc3QvbGlicmktbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUgICB7d2lkdGg6MTAwJTsgbWFyZ2luOjAgYXV0bzsgaGVpZ2h0OjUwMHB4OyBib3gtc2l6aW5nOiBib3JkZXItYm94O31cclxudHIsdGggICB7cGFkZGluZzowIDVweDsgYm94LXNpemluZzpib3JkZXItYm94OyBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlOyB0ZXh0LWFsaWduOiBjZW50ZXI7IGhlaWdodDoxMHB4O31cclxuLm9kZCAgICB7YmFja2dyb3VuZC1jb2xvcjojRkVGfVxyXG50cjpob3ZlciB7YmFja2dyb3VuZDogcmdiYSgwLDAsMCwwLjIpOyBib3gtc2l6aW5nOiBib3JkZXItYm94OyB9XHJcbnRkICAgICAgICB7aGVpZ2h0OiAxMHB4OyAgYm94LXNpemluZzogYm9yZGVyLWJveDsgfSJdfQ== */"

/***/ }),

/***/ "./src/app/libri/libri-list/libri-list.component.html":
/*!************************************************************!*\
  !*** ./src/app/libri/libri-list/libri-list.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table>\n  <thead>\n    <tr>\n      <th>Autore </th>\n      <th>Titolo</th>\n      <th>Numero Pagine</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let u of libri; let idx = index\" [ngClass]=\"{'odd':!(idx%2)}\">\n      <td>\n             {{u.autore.nome}}       \n             {{u.autore.cognome}}\n      </td>\n      <td>\n          {{u.titolo}}\n      </td>\n      <td>\n        {{u.numPagine}}\n      </td>\n    </tr>\n  </tbody>\n</table>\n\n"

/***/ }),

/***/ "./src/app/libri/libri-list/libri-list.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/libri/libri-list/libri-list.component.ts ***!
  \**********************************************************/
/*! exports provided: LibriListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LibriListComponent", function() { return LibriListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_utenti_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/_services/utenti.service */ "./src/app/_services/utenti.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var LibriListComponent = /** @class */ (function () {
    function LibriListComponent(libService, http) {
        this.libService = libService;
        this.http = http;
        //this.libri=new Libri[];
    }
    LibriListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var url = this.libService.costruiscimiUnUrl('libri'); //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
        this.http.get(url).subscribe(function (pippo) {
            _this.libri = pippo; //casting da ingresso che sarà un ogetto ottenuto da json a quello che voglio che sia, ovvero utente.
        }, function (error) {
            console.log(error.message);
        });
    };
    LibriListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-libri-list',
            template: __webpack_require__(/*! ./libri-list.component.html */ "./src/app/libri/libri-list/libri-list.component.html"),
            styles: [__webpack_require__(/*! ./libri-list.component.css */ "./src/app/libri/libri-list/libri-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_utenti_service__WEBPACK_IMPORTED_MODULE_2__["UtentiService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], LibriListComponent);
    return LibriListComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "form { width: 250px; height:300px; position:absolute; top:20%; left:40%; border:2px solid black;}\r\ndiv       {box-sizing:border-box; }\r\ndiv:first-of-type   { margin-top: 40px; }\r\nspan   { width:100%; text-align:center; display: block;}\r\ndiv input   { width:180px; margin:5px 33px;}\r\nbutton      {margin:0 93.5px; margin-top:30px; }\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLFlBQVksRUFBRSxZQUFZLEVBQUUsaUJBQWlCLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxzQkFBc0IsQ0FBQztBQUNoRyxXQUFXLHFCQUFxQixFQUFFO0FBQ2xDLHNCQUFzQixnQkFBZ0IsRUFBRTtBQUN4QyxTQUFTLFVBQVUsRUFBRSxpQkFBaUIsRUFBRSxjQUFjLENBQUM7QUFDdkQsY0FBYyxXQUFXLEVBQUUsZUFBZSxDQUFDO0FBRTNDLGFBQWEsZUFBZSxFQUFFLGVBQWUsRUFBRSIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJmb3JtIHsgd2lkdGg6IDI1MHB4OyBoZWlnaHQ6MzAwcHg7IHBvc2l0aW9uOmFic29sdXRlOyB0b3A6MjAlOyBsZWZ0OjQwJTsgYm9yZGVyOjJweCBzb2xpZCBibGFjazt9XHJcbmRpdiAgICAgICB7Ym94LXNpemluZzpib3JkZXItYm94OyB9XHJcbmRpdjpmaXJzdC1vZi10eXBlICAgeyBtYXJnaW4tdG9wOiA0MHB4OyB9XHJcbnNwYW4gICB7IHdpZHRoOjEwMCU7IHRleHQtYWxpZ246Y2VudGVyOyBkaXNwbGF5OiBibG9jazt9XHJcbmRpdiBpbnB1dCAgIHsgd2lkdGg6MTgwcHg7IG1hcmdpbjo1cHggMzNweDt9XHJcblxyXG5idXR0b24gICAgICB7bWFyZ2luOjAgOTMuNXB4OyBtYXJnaW4tdG9wOjMwcHg7IH1cclxuIl19 */"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form #login=\"ngForm\">\n    <div>\n      <label for=\"username\"></label> <span> UserName: </span> \n      <input name=\"username\" id=\"username\" \n      [(ngModel)]=\"unome\" #username=\"ngModel\" required>  \n    </div>\n    <div>\n      <label for=\"password\"></label> <span> Password: </span>\n      <input type=\"password\" name=\"password\" id=\"password\" \n      [(ngModel)]=\"pssd\" #password=\"ngModel\" required>  \n    </div>\n   \n    <div>\n      <button [disabled]=\"!login.valid\" (click)='loginAuth()'> LOGIN </button>\n      \n    </div>\n  \n    </form> \n    \n  "

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _modules_utente__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_modules/utente */ "./src/app/_modules/utente.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_autenticazione_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_services/autenticazione.service */ "./src/app/_services/autenticazione.service.ts");





var LoginComponent = /** @class */ (function () {
    function LoginComponent(autenticazioneSer, rotta) {
        this.autenticazioneSer = autenticazioneSer;
        this.rotta = rotta;
        this.utenteLoged = new _modules_utente__WEBPACK_IMPORTED_MODULE_2__["Utente"];
    }
    LoginComponent.prototype.ngOnInit = function () {
        if (this.autenticazioneSer.loggato()) {
            this.rotta.navigate(['users']);
        }
    };
    LoginComponent.prototype.loginAuth = function () {
        var _this = this;
        this.autenticazioneSer.login(this.unome, this.pssd).subscribe(function (risp) {
            var autenticato = risp;
            if (autenticato) {
                _this.rotta.navigate(['users']);
            }
            else {
            }
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_autenticazione_service__WEBPACK_IMPORTED_MODULE_4__["AutenticazioneService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/nav-bar/nav-bar.component.css":
/*!***********************************************!*\
  !*** ./src/app/nav-bar/nav-bar.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a   {text-decoration:none;\r\n    color:#000; font-weight: bold; line-height:30px;\r\n   padding-left: 5px; }\r\np   {background-color: #FFF; margin:0; height:30px; }\r\np:nth-of-type(odd)   {background-color: rgb(193, 183, 209)}\r\nh2   {\r\n     font-size:18px;\r\n     font-weight: bold;\r\n     text-align: center;\r\n     padding-bottom: 10px;\r\n       }\r\np:hover {\r\n   background-color: rgba(0,0,0,0.8);\r\n\r\n}\r\np:hover a   { color:white;}\r\nfa-icon { padding-right:5px; display: inline-block; }\r\nspan            {width:40px; height:21px;  display: inline-block; }\r\nspan:nth-of-type(even)    { width:110px }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmF2LWJhci9uYXYtYmFyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsS0FBSyxvQkFBb0I7SUFDckIsVUFBVSxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQjtHQUNoRCxpQkFBaUIsRUFBRTtBQUN0QixLQUFLLHNCQUFzQixFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUU7QUFDcEQsc0JBQXNCLG9DQUFvQztBQUMxRDtLQUNLLGNBQWM7S0FDZCxpQkFBaUI7S0FDakIsa0JBQWtCO0tBQ2xCLG9CQUFvQjtPQUNsQjtBQUNQO0dBQ0csaUNBQWlDOztBQUVwQztBQUNBLGNBQWMsV0FBVyxDQUFDO0FBQzFCLFVBQVUsaUJBQWlCLEVBQUUscUJBQXFCLEVBQUU7QUFDcEQsaUJBQWlCLFVBQVUsRUFBRSxXQUFXLEdBQUcscUJBQXFCLEVBQUU7QUFDbEUsNEJBQTRCLFlBQVkiLCJmaWxlIjoic3JjL2FwcC9uYXYtYmFyL25hdi1iYXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImEgICB7dGV4dC1kZWNvcmF0aW9uOm5vbmU7XHJcbiAgICBjb2xvcjojMDAwOyBmb250LXdlaWdodDogYm9sZDsgbGluZS1oZWlnaHQ6MzBweDtcclxuICAgcGFkZGluZy1sZWZ0OiA1cHg7IH1cclxucCAgIHtiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGOyBtYXJnaW46MDsgaGVpZ2h0OjMwcHg7IH1cclxucDpudGgtb2YtdHlwZShvZGQpICAge2JhY2tncm91bmQtY29sb3I6IHJnYigxOTMsIDE4MywgMjA5KX1cclxuaDIgICB7XHJcbiAgICAgZm9udC1zaXplOjE4cHg7XHJcbiAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gICAgICAgfVxyXG5wOmhvdmVyIHtcclxuICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjgpO1xyXG5cclxufSBcclxucDpob3ZlciBhICAgeyBjb2xvcjp3aGl0ZTt9XHJcbmZhLWljb24geyBwYWRkaW5nLXJpZ2h0OjVweDsgZGlzcGxheTogaW5saW5lLWJsb2NrOyB9XHJcbnNwYW4gICAgICAgICAgICB7d2lkdGg6NDBweDsgaGVpZ2h0OjIxcHg7ICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IH1cclxuc3BhbjpudGgtb2YtdHlwZShldmVuKSAgICB7IHdpZHRoOjExMHB4IH0iXX0= */"

/***/ }),

/***/ "./src/app/nav-bar/nav-bar.component.html":
/*!************************************************!*\
  !*** ./src/app/nav-bar/nav-bar.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2> MENÙ </h2>\n<p  (click)=\"evento(1)\">\n <a><span> <fa-icon [icon]=\"['fas', 'address-card']\"></fa-icon> </span> <span> Lista utenti </span> </a>\n</p>\n<p  (click)=\"evento(2)\">\n <a><span> <fa-icon [icon]=\"['fas', 'book']\"></fa-icon></span> <span> Lista libri </span></a>\n</p>\n<p  (click)=\"evento(3)\">\n <a> <span> <fa-icon [icon]=\"['fas', 'arrows-alt-h']\"></fa-icon> </span><span> Lista prestiti </span></a>\n</p>\n"

/***/ }),

/***/ "./src/app/nav-bar/nav-bar.component.ts":
/*!**********************************************!*\
  !*** ./src/app/nav-bar/nav-bar.component.ts ***!
  \**********************************************/
/*! exports provided: NavBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavBarComponent", function() { return NavBarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var NavBarComponent = /** @class */ (function () {
    function NavBarComponent(navigatore) {
        this.navigatore = navigatore;
    }
    NavBarComponent.prototype.ngOnInit = function () {
    };
    NavBarComponent.prototype.evento = function (n) {
        switch (n) {
            case (1): {
                this.navigatore.navigate(['users']);
                break;
            }
            case (2): {
                this.navigatore.navigate(['books']);
                break;
            }
            case (3): {
                this.navigatore.navigate(['borrowed']);
                break;
            }
        }
    };
    NavBarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-nav-bar',
            template: __webpack_require__(/*! ./nav-bar.component.html */ "./src/app/nav-bar/nav-bar.component.html"),
            styles: [__webpack_require__(/*! ./nav-bar.component.css */ "./src/app/nav-bar/nav-bar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], NavBarComponent);
    return NavBarComponent;
}());



/***/ }),

/***/ "./src/app/prestiti/prestiti-list/prestiti-list.component.css":
/*!********************************************************************!*\
  !*** ./src/app/prestiti/prestiti-list/prestiti-list.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table   {width:100%; margin:0 auto; height:500px; box-sizing: border-box;}\r\ntr,th   {padding:0 5px; box-sizing:border-box; background-color:white; text-align: center; height:10px;}\r\n.odd    {background-color:#FEF}\r\ntr:hover {background: rgba(0,0,0,0.2); box-sizing: border-box; }\r\ntd        {height: 10px;  box-sizing: border-box; }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJlc3RpdGkvcHJlc3RpdGktbGlzdC9wcmVzdGl0aS1saXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsU0FBUyxVQUFVLEVBQUUsYUFBYSxFQUFFLFlBQVksRUFBRSxzQkFBc0IsQ0FBQztBQUN6RSxTQUFTLGFBQWEsRUFBRSxxQkFBcUIsRUFBRSxzQkFBc0IsRUFBRSxrQkFBa0IsRUFBRSxXQUFXLENBQUM7QUFDdkcsU0FBUyxxQkFBcUI7QUFDOUIsVUFBVSwyQkFBMkIsRUFBRSxzQkFBc0IsRUFBRTtBQUMvRCxXQUFXLFlBQVksR0FBRyxzQkFBc0IsRUFBRSIsImZpbGUiOiJzcmMvYXBwL3ByZXN0aXRpL3ByZXN0aXRpLWxpc3QvcHJlc3RpdGktbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUgICB7d2lkdGg6MTAwJTsgbWFyZ2luOjAgYXV0bzsgaGVpZ2h0OjUwMHB4OyBib3gtc2l6aW5nOiBib3JkZXItYm94O31cclxudHIsdGggICB7cGFkZGluZzowIDVweDsgYm94LXNpemluZzpib3JkZXItYm94OyBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlOyB0ZXh0LWFsaWduOiBjZW50ZXI7IGhlaWdodDoxMHB4O31cclxuLm9kZCAgICB7YmFja2dyb3VuZC1jb2xvcjojRkVGfVxyXG50cjpob3ZlciB7YmFja2dyb3VuZDogcmdiYSgwLDAsMCwwLjIpOyBib3gtc2l6aW5nOiBib3JkZXItYm94OyB9XHJcbnRkICAgICAgICB7aGVpZ2h0OiAxMHB4OyAgYm94LXNpemluZzogYm9yZGVyLWJveDsgfSJdfQ== */"

/***/ }),

/***/ "./src/app/prestiti/prestiti-list/prestiti-list.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/prestiti/prestiti-list/prestiti-list.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table>\n  <thead>\n    <tr>\n        <th>Utente</th>\n        <th> Libro </th>\n      <th>Inizio Prestito </th>\n      <th>Fine Prestito </th>\n    \n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let u of prestiti; let idx = index\" [ngClass]=\"{'odd':!(idx%2)}\">\n      <td>\n             {{u.utente.nome}}       \n             {{u.utente.cognome}}\n      </td>\n      <td>\n          {{u.libro.titolo}}\n      </td>\n      <td>\n        {{u.dataInizio}}\n      </td>\n      <td>\n          {{u.dataFine}}\n        </td>\n    </tr>\n  </tbody>\n</table>\n\n"

/***/ }),

/***/ "./src/app/prestiti/prestiti-list/prestiti-list.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/prestiti/prestiti-list/prestiti-list.component.ts ***!
  \*******************************************************************/
/*! exports provided: PrestitiListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrestitiListComponent", function() { return PrestitiListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_utenti_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/_services/utenti.service */ "./src/app/_services/utenti.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var PrestitiListComponent = /** @class */ (function () {
    function PrestitiListComponent(prestService, http) {
        this.prestService = prestService;
        this.http = http;
    }
    PrestitiListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var url = this.prestService.costruiscimiUnUrl('prestiti'); //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
        this.http.get(url).subscribe(function (pippo) {
            _this.prestiti = pippo; //casting da ingresso che sarà un ogetto ottenuto da json a quello che voglio che sia, ovvero utente.
        }, function (error) {
            console.log(error.message);
        });
    };
    PrestitiListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-prestiti-list',
            template: __webpack_require__(/*! ./prestiti-list.component.html */ "./src/app/prestiti/prestiti-list/prestiti-list.component.html"),
            styles: [__webpack_require__(/*! ./prestiti-list.component.css */ "./src/app/prestiti/prestiti-list/prestiti-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_utenti_service__WEBPACK_IMPORTED_MODULE_2__["UtentiService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], PrestitiListComponent);
    return PrestitiListComponent;
}());



/***/ }),

/***/ "./src/app/router-module/router-module.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/router-module/router-module.module.ts ***!
  \*******************************************************/
/*! exports provided: RouterModuleModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouterModuleModule", function() { return RouterModuleModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _utenti_usr_list_usr_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utenti/usr-list/usr-list.component */ "./src/app/utenti/usr-list/usr-list.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _utenti_usr_detail_usr_detail_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utenti/usr-detail/usr-detail.component */ "./src/app/utenti/usr-detail/usr-detail.component.ts");
/* harmony import */ var _services_guard_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_services/guard.service */ "./src/app/_services/guard.service.ts");
/* harmony import */ var _libri_libri_list_libri_list_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../libri/libri-list/libri-list.component */ "./src/app/libri/libri-list/libri-list.component.ts");
/* harmony import */ var _prestiti_prestiti_list_prestiti_list_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../prestiti/prestiti-list/prestiti-list.component */ "./src/app/prestiti/prestiti-list/prestiti-list.component.ts");










var rotte = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login',
        canActivate: [_services_guard_service__WEBPACK_IMPORTED_MODULE_7__["GuardService"]],
    },
    {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"],
    },
    {
        path: 'users',
        component: _utenti_usr_list_usr_list_component__WEBPACK_IMPORTED_MODULE_4__["UsrListComponent"],
        canActivate: [_services_guard_service__WEBPACK_IMPORTED_MODULE_7__["GuardService"]],
    }, {
        path: 'users/:id/edit',
        component: _utenti_usr_detail_usr_detail_component__WEBPACK_IMPORTED_MODULE_6__["UsrDetailComponent"],
        canActivate: [_services_guard_service__WEBPACK_IMPORTED_MODULE_7__["GuardService"]],
    }, {
        path: 'books',
        component: _libri_libri_list_libri_list_component__WEBPACK_IMPORTED_MODULE_8__["LibriListComponent"],
        canActivate: [_services_guard_service__WEBPACK_IMPORTED_MODULE_7__["GuardService"]],
    }, {
        path: 'borrowed',
        component: _prestiti_prestiti_list_prestiti_list_component__WEBPACK_IMPORTED_MODULE_9__["PrestitiListComponent"],
        canActivate: [_services_guard_service__WEBPACK_IMPORTED_MODULE_7__["GuardService"]],
    }
];
var RouterModuleModule = /** @class */ (function () {
    function RouterModuleModule() {
    }
    RouterModuleModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(rotte)
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]
            ]
        })
    ], RouterModuleModule);
    return RouterModuleModule;
}());



/***/ }),

/***/ "./src/app/utenti/usr-detail/usr-detail.component.css":
/*!************************************************************!*\
  !*** ./src/app/utenti/usr-detail/usr-detail.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3V0ZW50aS91c3ItZGV0YWlsL3Vzci1kZXRhaWwuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/utenti/usr-detail/usr-detail.component.html":
/*!*************************************************************!*\
  !*** ./src/app/utenti/usr-detail/usr-detail.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"semaforo | async as mostraForm\">\n  <div #mostraForm>\n    <form #dettaglioUsr=\"ngForm\">\n    <div>\n      <label for=\"nome\"></label> <span>Nome</span>\n      <input name=\"nome\" id=\"nome\" [(ngModel)]=\"usr.nome\" #nome=\"ngModel\" required>  \n    </div>\n    <div>\n      <label for=\"cognome\"></label> <span> Cognome </span>\n      <input name=\"cognome\" id=\"cognome\" [(ngModel)]=\"usr.cognome\" #cognome=\"ngModel\" required>  \n    </div>\n    <div>\n      <label for=\"username\"></label> <span> UserName </span>\n      <input name=\"username\" id=\"username\" [(ngModel)]=\"usr.username\" #username=\"ngModel\" required>  \n    </div>\n    <div>\n      <label for=\"password\"></label> <span> Password </span>\n      <input type=\"password\" name=\"password\" id=\"password\" [(ngModel)]=\"usr.password\" #password=\"ngModel\" required>  \n    </div>\n    <div>\n        <label for=\"tessera\"></label> <span> Tessera </span> \n        <input name=\"tessera\" id=\"tessera\" [(ngModel)]=\"usr.tessera\" #tessera=\"ngModel\" required>  \n    </div>\n    <div>\n     <div > <button [disabled]=\"!dettaglioUsr.valid\" (click)=\"saveUsr()\"> SAVE </button> </div>\n     <div *ngIf=\"ad\"> <button [disabled]=\"!dettaglioUsr.valid\" (click)=\"delete()\"> CANCELLA </button> </div>\n    </div>\n  \n    </form> \n  </div>\n  <ng-template>\n    loading\n  </ng-template>  \n  </div>\n  \n<button (click)=\"indietro()\"> indietro </button>\n"

/***/ }),

/***/ "./src/app/utenti/usr-detail/usr-detail.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/utenti/usr-detail/usr-detail.component.ts ***!
  \***********************************************************/
/*! exports provided: UsrDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsrDetailComponent", function() { return UsrDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_utenti_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/_services/utenti.service */ "./src/app/_services/utenti.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_modules_utente__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/_modules/utente */ "./src/app/_modules/utente.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var src_app_services_autenticazione_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/_services/autenticazione.service */ "./src/app/_services/autenticazione.service.ts");






//import { Ruolo } from 'src/app/_modules/ruolo';

var UsrDetailComponent = /** @class */ (function () {
    function UsrDetailComponent(usrService, rotta, rottaAttiva, aut) {
        this.usrService = usrService;
        this.rotta = rotta;
        this.rottaAttiva = rottaAttiva;
        this.aut = aut;
        this.usr = new src_app_modules_utente__WEBPACK_IMPORTED_MODULE_4__["Utente"]();
        this.usrDEEPcopy = new src_app_modules_utente__WEBPACK_IMPORTED_MODULE_4__["Utente"]();
        this.ad = false;
    }
    UsrDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.rottaAttiva.params.subscribe(function (params) {
            _this.usrService.cercaConId(+params.id).subscribe(function (utente) {
                _this.usrDEEPcopy = utente;
                _this.usr = Object.assign({}, _this.usrDEEPcopy);
                _this.aut.ottieniDaMemoria().ruoli.forEach(function (ruolo) {
                    if (ruolo.codice === 'ROLE_ADMIN')
                        _this.ad = true;
                });
                _this.semaforo = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Observable"](function (observer) {
                    observer.next(true);
                    observer.complete;
                });
            }, function (error) {
            });
        });
    };
    UsrDetailComponent.prototype.indietro = function () {
        var _this = this;
        this.rotta.navigate(['users']);
        this.usrService.editUsr(this.usrDEEPcopy).subscribe(function (utenti) {
            _this.usrDEEPcopy = utenti;
            _this.rotta.navigate(['users']);
        });
    };
    UsrDetailComponent.prototype.saveUsr = function () {
        var _this = this;
        this.usrService.editUsr(this.usr).subscribe(function (utenti) {
            _this.usr = utenti;
            _this.rotta.navigate(['users']);
        });
    };
    UsrDetailComponent.prototype.delete = function () {
        var _this = this;
        this.usrService.deleteUsr(this.usr).subscribe(function (utenti) {
            _this.usr = utenti;
            _this.rotta.navigate(['users']);
        });
    };
    UsrDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-usr-detail',
            template: __webpack_require__(/*! ./usr-detail.component.html */ "./src/app/utenti/usr-detail/usr-detail.component.html"),
            styles: [__webpack_require__(/*! ./usr-detail.component.css */ "./src/app/utenti/usr-detail/usr-detail.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_utenti_service__WEBPACK_IMPORTED_MODULE_2__["UtentiService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            src_app_services_autenticazione_service__WEBPACK_IMPORTED_MODULE_6__["AutenticazioneService"]])
    ], UsrDetailComponent);
    return UsrDetailComponent;
}());



/***/ }),

/***/ "./src/app/utenti/usr-list/usr-list.component.css":
/*!********************************************************!*\
  !*** ./src/app/utenti/usr-list/usr-list.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table   {width:100%; margin:0 auto; height:500px; box-sizing: border-box;}\r\ntr,th   {padding:0 5px; box-sizing:border-box; background-color:white; text-align: center; height:10px;}\r\n.odd    {background-color:#FEF}\r\ntr:hover {background: rgba(0,0,0,0.2); box-sizing: border-box; }\r\ntd        {height: 10px;  box-sizing: border-box; }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXRlbnRpL3Vzci1saXN0L3Vzci1saXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsU0FBUyxVQUFVLEVBQUUsYUFBYSxFQUFFLFlBQVksRUFBRSxzQkFBc0IsQ0FBQztBQUN6RSxTQUFTLGFBQWEsRUFBRSxxQkFBcUIsRUFBRSxzQkFBc0IsRUFBRSxrQkFBa0IsRUFBRSxXQUFXLENBQUM7QUFDdkcsU0FBUyxxQkFBcUI7QUFDOUIsVUFBVSwyQkFBMkIsRUFBRSxzQkFBc0IsRUFBRTtBQUMvRCxXQUFXLFlBQVksR0FBRyxzQkFBc0IsRUFBRSIsImZpbGUiOiJzcmMvYXBwL3V0ZW50aS91c3ItbGlzdC91c3ItbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUgICB7d2lkdGg6MTAwJTsgbWFyZ2luOjAgYXV0bzsgaGVpZ2h0OjUwMHB4OyBib3gtc2l6aW5nOiBib3JkZXItYm94O31cclxudHIsdGggICB7cGFkZGluZzowIDVweDsgYm94LXNpemluZzpib3JkZXItYm94OyBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlOyB0ZXh0LWFsaWduOiBjZW50ZXI7IGhlaWdodDoxMHB4O31cclxuLm9kZCAgICB7YmFja2dyb3VuZC1jb2xvcjojRkVGfVxyXG50cjpob3ZlciB7YmFja2dyb3VuZDogcmdiYSgwLDAsMCwwLjIpOyBib3gtc2l6aW5nOiBib3JkZXItYm94OyB9XHJcbnRkICAgICAgICB7aGVpZ2h0OiAxMHB4OyAgYm94LXNpemluZzogYm9yZGVyLWJveDsgfSJdfQ== */"

/***/ }),

/***/ "./src/app/utenti/usr-list/usr-list.component.html":
/*!*********************************************************!*\
  !*** ./src/app/utenti/usr-list/usr-list.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #loaded>\n    <table>\n      <thead>\n        <tr>\n          <th>ID</th>\n          <th>NOME COGNOME</th>\n          <th>TESSERA</th>\n          <th>USERNAME</th>\n          <th>FUNZIONALITA'</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor=\"let u of utenti; let idx = index\" [ngClass]=\"{'odd':!(idx%2)}\">\n          <td>\n            {{u.id}}\n          </td>\n          <td>\n         \n            {{u.nome}}\n          \n            {{u.cognome}}\n          \n          </td>\n          <td>\n            {{u.tessera}}\n          </td>\n          <td>\n            {{u.username}}\n          </td>\n          <td>\n            <button  (click)=\"dettaglioUsr(u.id)\"> EDIT </button>\n            <button [hidden]=\"!ad\"  (click)=\"delete(u)\" > DELETE </button>\n          </td>\n        </tr>\n      </tbody>\n    </table>\n    <button (click)=\"newUsr()\"> New User </button>\n</div>\n  "

/***/ }),

/***/ "./src/app/utenti/usr-list/usr-list.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/utenti/usr-list/usr-list.component.ts ***!
  \*******************************************************/
/*! exports provided: UsrListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsrListComponent", function() { return UsrListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_utenti_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/_services/utenti.service */ "./src/app/_services/utenti.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var src_app_services_autenticazione_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/_services/autenticazione.service */ "./src/app/_services/autenticazione.service.ts");






var UsrListComponent = /** @class */ (function () {
    function UsrListComponent(rotta, usrService, aut) {
        this.rotta = rotta;
        this.usrService = usrService;
        this.aut = aut;
        this.ad = false;
        //this.utenti=new Utente[];
    }
    UsrListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.usrService.ottieniListUtenti('utenti').subscribe(function (usr) {
            _this.utenti = usr;
            var roles = _this.aut.ottieniDaMemoria().ruoli;
            roles.forEach(function (ruolo) {
                if (ruolo.codice === 'ROLE_ADMIN')
                    _this.ad = true;
            });
        }, function (error) {
            console.log('errore lista non caricata');
        });
    };
    UsrListComponent.prototype.dettaglioUsr = function (id) {
        this.rotta.navigate(['users/' + id + '/edit']);
    };
    UsrListComponent.prototype.delete = function (usr) {
        var _this = this;
        this.usrService.deleteUsr(usr).subscribe(function (utenti) {
            usr = utenti;
            _this.semaforo = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Observable"](function (observer) {
                observer.next(true);
                observer.complete();
            });
            _this.reload();
            _this.rotta.navigate(['users']);
        });
    };
    UsrListComponent.prototype.reload = function () {
        var _this = this;
        this.usrService.ottieniListUtenti('utenti').subscribe(function (usr) {
            _this.utenti = usr;
            _this.usrService.autenticato.ruoli.forEach(function (ruolo) {
                if (ruolo.codice === 'ROLE_ADMIN')
                    _this.ad = true;
            });
        }, function (error) {
            console.log('errore lista non caricata');
        });
    };
    UsrListComponent.prototype.newUsr = function () {
    };
    UsrListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-usr-list',
            template: __webpack_require__(/*! ./usr-list.component.html */ "./src/app/utenti/usr-list/usr-list.component.html"),
            styles: [__webpack_require__(/*! ./usr-list.component.css */ "./src/app/utenti/usr-list/usr-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], src_app_services_utenti_service__WEBPACK_IMPORTED_MODULE_3__["UtentiService"], src_app_services_autenticazione_service__WEBPACK_IMPORTED_MODULE_5__["AutenticazioneService"]])
    ], UsrListComponent);
    return UsrListComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\CORSO 55\Documents\corsoFE\esercitazioni\angular\libCasa\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map