import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core'


import { AppComponent } from './app.component';
import { UsrListComponent } from './utenti/usr-list/usr-list.component';
import { LoginComponent } from './login/login.component';
import { RouterModuleModule } from './router-module/router-module.module';
import { HttpClientModule } from '@angular/common/http'
import { UtentiService } from './_services/utenti.service';
import { UsrDetailComponent } from './utenti/usr-detail/usr-detail.component';
import { AutenticazioneService } from './_services/autenticazione.service';
import { LibriListComponent } from './libri/libri-list/libri-list.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { PrestitiListComponent } from './prestiti/prestiti-list/prestiti-list.component';

@NgModule({
  declarations: [
    AppComponent,
    UsrListComponent,
    LoginComponent,
    UsrDetailComponent,
    LibriListComponent,
    NavBarComponent,
    PrestitiListComponent
  ],
  imports: [
    FontAwesomeModule,
    FormsModule,
    HttpClientModule,
    RouterModuleModule,
    BrowserModule
  ],
  providers: [UtentiService, AutenticazioneService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(){
   
    library.add(fas);
 }
}
