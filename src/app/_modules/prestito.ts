import { Utente } from './utente';
import { Libri } from './libri';

export class Prestito {
    id:string;
    dataInizio:string;
    dataFine:string;
    utente:Utente;
    libro:Libri;

}

/*
 {
      "id": 3,
      "dataInizio": "15/03/2019",
      "dataFine": "15/04/2019",
      "utente": {
        "id": 1,
        "nome": "Mario",
        "cognome": "Rossi",
        "tessera": "Tessera 0"
      },
      "libro": {
        "id": 3,
        "titolo": "Hyperion"
      }
    }*/