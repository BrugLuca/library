import { Ruolo } from './ruolo';

export class Utente {
    
    id:number;
    nome: string;
    cognome: string;
    tessera: string;
    username:string;
    password:string;
    token:string;
    ruoli:Ruolo[];

    constructor(){

    }
}

