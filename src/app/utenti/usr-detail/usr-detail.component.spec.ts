import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsrDetailComponent } from './usr-detail.component';

describe('UsrDetailComponent', () => {
  let component: UsrDetailComponent;
  let fixture: ComponentFixture<UsrDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsrDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsrDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
