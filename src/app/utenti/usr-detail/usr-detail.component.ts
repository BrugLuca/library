import { Component, OnInit } from '@angular/core';
import { UtentiService } from 'src/app/_services/utenti.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Utente } from 'src/app/_modules/utente';
import { Observable } from 'rxjs';
//import { Ruolo } from 'src/app/_modules/ruolo';
import { AutenticazioneService } from 'src/app/_services/autenticazione.service';

@Component({
  selector: 'app-usr-detail',
  templateUrl: './usr-detail.component.html',
  styleUrls: ['./usr-detail.component.css']
})
export class UsrDetailComponent implements OnInit {
  usr:Utente;
  
  usrDEEPcopy:Utente
  ad:boolean;
  semaforo:Observable<boolean>;
  constructor(private usrService:UtentiService,private rotta: Router, private rottaAttiva: ActivatedRoute,
     private aut:AutenticazioneService) {
    this.usr=new Utente();
    this.usrDEEPcopy=new Utente();
    this.ad=false;
    }

  ngOnInit() {
    this.rottaAttiva.params.subscribe(
      (params) =>{
      this.usrService.cercaConId(+params.id).subscribe(
        utente =>{
          this.usrDEEPcopy=(<Utente>utente);
          this.usr = Object.assign({}, this.usrDEEPcopy);
          this.aut.ottieniDaMemoria().ruoli.forEach(
            ruolo=>{
              if(ruolo.codice ==='ROLE_ADMIN')
              this.ad=true;
            }
          );
          this.semaforo = new Observable((observer) =>
          {
            observer.next(true);
            observer.complete;
          }
          );

        },
        error => {

        }
      );

      }
    )

  }


  indietro()
  {
    this.rotta.navigate(['users']);
    this.usrService.editUsr(this.usrDEEPcopy).subscribe(
      utenti => {
        this.usrDEEPcopy=(<Utente> utenti);
        this.rotta.navigate(['users']);
      }
    )
  }

  saveUsr()
  {
    this.usrService.editUsr(this.usr).subscribe(
      utenti => {
        this.usr=(<Utente> utenti);
        this.rotta.navigate(['users']);

      }
    )
  }
  delete(){
    this.usrService.deleteUsr(this.usr).subscribe(
      utenti =>{
        this.usr=(<Utente> utenti);
        this.rotta.navigate(['users']);
      }
    )
  }
  
}
