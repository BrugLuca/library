import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtentiService } from 'src/app/_services/utenti.service';
import { Utente } from 'src/app/_modules/utente';
import { Observable } from 'rxjs';
import { AutenticazioneService } from 'src/app/_services/autenticazione.service';
import { Ruolo } from 'src/app/_modules/ruolo';

@Component({
  selector: 'app-usr-list',
  templateUrl: './usr-list.component.html',
  styleUrls: ['./usr-list.component.css']
})
export class UsrListComponent implements OnInit {
  utenti:Utente[];
  ad:boolean=false;
  semaforo:Observable<boolean>;

  constructor(private rotta: Router, private usrService:UtentiService, private aut:AutenticazioneService ) { 
    //this.utenti=new Utente[];
  }

  ngOnInit() {
    this.usrService.ottieniListUtenti('utenti').subscribe(
      usr=> {
        this.utenti=(<Utente[]> usr);
        const roles: Ruolo[] = this.aut.ottieniDaMemoria().ruoli;
        roles.forEach(
          ruolo=>{
            if(ruolo.codice ==='ROLE_ADMIN')
            this.ad=true;
          }
        );
      
      },
      error => {
        console.log('errore lista non caricata')
      }
    );
  }


  dettaglioUsr(id:number){
    this.rotta.navigate(['users/'+ id +'/edit']);
  }
  delete(usr:Utente){
    this.usrService.deleteUsr(usr).subscribe(
      utenti =>{
        usr=(<Utente> utenti);
        this.semaforo = new Observable((observer) =>
          {
            observer.next(true);
            observer.complete();
          }
          );
          this.reload();
        this.rotta.navigate(['users']);

      }
    )
  }
  reload(){
    this.usrService.ottieniListUtenti('utenti').subscribe(
      usr=> {
        this.utenti=(<Utente[]> usr);
        this.usrService.autenticato.ruoli.forEach(
          ruolo =>{
            if(ruolo.codice==='ROLE_ADMIN')
            this.ad=true;
          }
        )
      
      },
      error => {
        console.log('errore lista non caricata')
      }
    );

  }
  newUsr(){

  }
}
