import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsrListComponent } from './usr-list.component';

describe('UsrListComponent', () => {
  let component: UsrListComponent;
  let fixture: ComponentFixture<UsrListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsrListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsrListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
