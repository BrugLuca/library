import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import { UsrListComponent } from '../utenti/usr-list/usr-list.component';
import { LoginComponent } from '../login/login.component';
import { UsrDetailComponent } from '../utenti/usr-detail/usr-detail.component';
import { GuardService } from '../_services/guard.service';
import { LibriListComponent } from '../libri/libri-list/libri-list.component';
import { PrestitiListComponent } from '../prestiti/prestiti-list/prestiti-list.component';


const rotte: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login',
    canActivate: [GuardService],
   },
  {
    path: 'login',
    component: LoginComponent,
  
  },
  {
    path: 'users',
    component: UsrListComponent,
   canActivate: [GuardService],
  }, {
    path: 'users/:id/edit',
    component: UsrDetailComponent,
    canActivate: [GuardService],
  }, {
    path: 'books',
    component: LibriListComponent,
    canActivate: [GuardService],
  },{
    path:'borrowed',
    component: PrestitiListComponent,
    canActivate: [GuardService],
  }
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(rotte)
  ],
  exports: [
    RouterModule
  ]
})
export class RouterModuleModule { }
