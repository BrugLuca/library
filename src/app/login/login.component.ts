import { Component, OnInit } from '@angular/core';
import { UtentiService } from '../_services/utenti.service';
import { Utente } from '../_modules/utente';
import { Route, Router } from '@angular/router';
import { AutenticazioneService } from '../_services/autenticazione.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public unome:string;
  public pssd:string;
  utenteLoged:Utente;
  constructor( private autenticazioneSer:AutenticazioneService, private rotta:Router) { 
    this.utenteLoged= new Utente;
  }

  ngOnInit() {
    if(this.autenticazioneSer.loggato()){
      this.rotta.navigate(['users']);
    }
  }

  loginAuth()
  {
    this.autenticazioneSer.login(this.unome, this.pssd).subscribe(
      risp=>{
        const autenticato=risp;
        if (autenticato){
          this.rotta.navigate(['users']);

        }
        else{
          
        }
      }
    );

  }
}
