import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Utente } from '../_modules/utente';


@Injectable({
  providedIn: 'root'
})
export class UtentiService extends BaseService {
 autenticato:Utente;
 token:boolean;
  constructor(private http:HttpClient) {
    super();
    this.autenticato=new Utente();
    this.token=false;
   }

   public ottieniListUtenti(quale:string)
   {
     const url = this.costruiscimiUnUrl(quale);
      return this.http.get(url);
   }
   public cercaConId(id: number)
   {
     return this.ottieniListUtenti('utenti/' +id);
   }
   public logging( user:string, pss:string)
   {
     const url = this.costruiscimiUnUrl('utenti?username=' + user + '&password=' +pss );
     return this.http.get(url);
   }

   public editUsr(usr:Utente){
     const url=this.costruiscimiUnUrl('utenti/'+usr.id);
     return this.http.put(url, usr);
   }

   public deleteUsr(usr:Utente){
     const url=this.costruiscimiUnUrl('utenti/'+usr.id); 
     return this.http.delete(url);
   }

}
