import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  urlServer='http://localhost:3000/';

  constructor() { }

  public costruiscimiUnUrl(urlInfo:string)
  {
    return this.urlServer+urlInfo;
  }
}
