import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { AutenticazioneService } from './autenticazione.service';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {

  constructor(private navigatore: Router, private autServ:AutenticazioneService) { 

  }

  canActivate(rotta: ActivatedRouteSnapshot, stato:RouterStateSnapshot){
    if(this.autServ.loggato()){
      const rottaVera = rotta.routeConfig.path;
      if (rottaVera ==='/'){
        this.navigatore.navigate(['users']);
      }
      return true;
    }
    alert('errore');
    this.navigatore.navigate(['login']);
    return false;

  }

}
