import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Utente } from '../_modules/utente';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutenticazioneService extends BaseService{

  constructor(private http: HttpClient) {
    super();
   }

  login(usr:string, pss:string): Observable<boolean>{
    let utenteSessione: Utente = null;
    const auth = new Observable<boolean>(
      (observer)=>{
        const url = this.costruiscimiUnUrl('utenti?username=' + usr + '&password=' + pss);
        this.http.get(url).subscribe(
          risposta=>{
            if(risposta[0]){
              utenteSessione=(<Utente> risposta[0])
              this.memorizzaSessioneUsr(utenteSessione);
              observer.next(true);
              observer.complete();
            }
            else{
              observer.next(false);
              observer.complete();
            }
          }
        );
      }  
    );
    return auth;
   
  }

  private memorizzaSessioneUsr(usr:Utente){
    localStorage.setItem('currentUser', JSON.stringify(usr));
    
  }

  ottieniDaMemoria():Utente{
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  loggato():boolean{
    const utente:Utente = this.ottieniDaMemoria();
    if (utente && utente.token){
      return true;
    } else{
      return false;
    }

  }

  logout() {
    localStorage.removeItem('currentUser');
  }

}
