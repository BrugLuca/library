import { Component, OnInit } from '@angular/core';
import { UtentiService } from './_services/utenti.service';
import { AutenticazioneService } from './_services/autenticazione.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'libProject';
  autUsr:boolean=false;
  constructor(private autenticazione: AutenticazioneService, private rotta:Router){
    this.autUsr = this.autenticazione.loggato();
  }

  ngOnInit() {
   

  }
  logout(){
    this.autenticazione.logout();
    this.rotta.navigate(['login']);
  }

  onActivate() {
    this.autUsr = this.autenticazione.loggato();
  }
}

