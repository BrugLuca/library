import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(private navigatore:Router) { }

  ngOnInit() {
  }

  evento(n:number){
    
    switch(n){
     
      case (1) :{
        this.navigatore.navigate(['users']);
        break;
      }
      case (2):{
        this.navigatore.navigate(['books']);
        break;

    }
      case (3):{
        this.navigatore.navigate(['borrowed']);
        break;
        
        

       }

      }
     }
  
}
