import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrestitiListComponent } from './prestiti-list.component';

describe('PrestitiListComponent', () => {
  let component: PrestitiListComponent;
  let fixture: ComponentFixture<PrestitiListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrestitiListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrestitiListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
