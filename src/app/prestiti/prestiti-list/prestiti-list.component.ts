import { Component, OnInit } from '@angular/core';
import { UtentiService } from 'src/app/_services/utenti.service';
import { HttpClient } from '@angular/common/http';
import { Prestito } from 'src/app/_modules/prestito';

@Component({
  selector: 'app-prestiti-list',
  templateUrl: './prestiti-list.component.html',
  styleUrls: ['./prestiti-list.component.css']
})
export class PrestitiListComponent implements OnInit {

  prestiti:Prestito[];

  constructor(private prestService:UtentiService, private http: HttpClient) { }

  ngOnInit() {
    const url = this.prestService.costruiscimiUnUrl('prestiti'); //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
    this.http.get(url).subscribe(
    pippo => {
      this.prestiti=(<Prestito[]>pippo); //casting da ingresso che sarà un ogetto ottenuto da json a quello che voglio che sia, ovvero utente.
      
    },
    error => {
      console.log(error.message);
    }
  );

  }

  }


