import { Component, OnInit } from '@angular/core';
import { UtentiService } from 'src/app/_services/utenti.service';
import { HttpClient } from '@angular/common/http';
import { Libri } from 'src/app/_modules/libri';

@Component({
  selector: 'app-libri-list',
  templateUrl: './libri-list.component.html',
  styleUrls: ['./libri-list.component.css']
})
export class LibriListComponent implements OnInit {

  libri:Libri[];


  constructor(private libService: UtentiService, private http: HttpClient) { 
    //this.libri=new Libri[];
  }

  ngOnInit() {
    const url = this.libService.costruiscimiUnUrl('libri'); //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
    this.http.get(url).subscribe(
    pippo => {
      this.libri=(<Libri[]>pippo); //casting da ingresso che sarà un ogetto ottenuto da json a quello che voglio che sia, ovvero utente.
      
    },
    error => {
      console.log(error.message);
    }
  );

  }

}
